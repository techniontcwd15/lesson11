import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AttributeDirectiveDirective } from './attribute-directive.directive';
import { StractureDirectiveDirective } from './stracture-directive.directive';

@NgModule({
  declarations: [
    AppComponent,
    AttributeDirectiveDirective,
    StractureDirectiveDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
