import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appStractureDirective]'
})
export class StractureDirectiveDirective {
  private hasView = false;

  @Input() set appStractureDirective(condition: boolean) {
    if (!condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      // this.viewContainer.createComponent();
      this.hasView = true;
    } else if (condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }

  constructor(private templateRef: TemplateRef<any>,private viewContainer: ViewContainerRef) { }

}
