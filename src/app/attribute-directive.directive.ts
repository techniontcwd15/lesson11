import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAttributeDirective]'
})
export class AttributeDirectiveDirective {

  constructor(private el:ElementRef) {
    el.nativeElement.style.background = 'red';
  }

}
